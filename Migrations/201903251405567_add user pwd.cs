namespace TodoApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adduserpwd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.Memos", "Description", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Users", "Username", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.Users", "Name", c => c.String(nullable: false, maxLength: 32));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Name", c => c.String());
            AlterColumn("dbo.Users", "Username", c => c.String());
            AlterColumn("dbo.Memos", "Description", c => c.String());
            DropColumn("dbo.Users", "Password");
        }
    }
}
