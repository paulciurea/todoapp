﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoApp.Models
{
    public class Memo
    {
        [Key]
        public int ID { set; get; }

        [Required]
        [MinLength(1)]
        [MaxLength(255)]
        public string Description { set; get; }

        [Required]
        [Display(AutoGenerateField = false)]
        [DataType(DataType.DateTime)]
        public DateTime CreationTime { set; get; }

        [Display(Name = "Due To")]
        [DataType(DataType.DateTime)]
        public DateTime TargetTime { set; get; }
    }
}