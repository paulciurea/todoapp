﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TodoApp.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(): base()
        {

        }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Memo> Memos { get; set; }

    }
}