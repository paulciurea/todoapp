﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoApp.Models
{
    public class User
    {
        [Key]
        public int ID { set; get; }

        [Required]
        [MinLength(8)]
        [MaxLength(32)]
        public string Username { set; get; }

        [Required]
        [MinLength(8)]
        [MaxLength(32)]
        public string Password { set; get; }

        [Required]
        [MinLength(4)]
        [MaxLength(32)]
        public string Name { set; get; }

        public virtual List<Memo> Memos { set; get; }
    }
}